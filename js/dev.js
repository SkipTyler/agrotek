'use strict';


// modal
(function () {
	const _modalBtn = document.querySelectorAll('.js-modalBtn');
	const _body = document.querySelector('body');
	// open modal
	if (_modalBtn) {
		Array.prototype.forEach.call(_modalBtn, el => {
			const _modalId = el.getAttribute('data-modalId');
			el.addEventListener('click', ev => {
				ev.preventDefault();
				const  _modal = document.querySelector('.js-modal' + _modalId);
				_modal.classList.add('active');
				const _scrollWidth = document.documentElement.scrollWidth;
				const _screenWidth = screen.width;
				const _rightPadding = _screenWidth - _scrollWidth;
				_body.style.paddingRight = `${_rightPadding}` + 'px';
				_body.classList.add('no-scroll');
				document.documentElement.classList.add('no-scroll-el');
			})
		});
		//close modal
		const _modals = document.querySelectorAll('.js-modal');
		Array.prototype.forEach.call(_modals, el => {
			el.addEventListener('click', ev => {
				const _target = ev.target || ev.currentTarget;
				if (_target.classList.contains('modal--wrap') || _target.classList.contains('js-closeModal')) {
					el.classList.remove('active');
					_body.removeAttribute('style');
					_body.classList.remove('no-scroll');
					document.documentElement.classList.remove('no-scroll-el');
				}
			})
		})
	}
})();

//counter
(function () {
	const animateValue = (obj) => {
		const _start = 0;
		const _duration = 2000;
		const _parent = document.querySelectorAll(obj);
		Array.prototype.forEach.call(_parent, el => {
			const _end = el.getAttribute('data-counter');
			const _range = _end - _start;
			let _current = _start;
			const _increment = _end > _start? 1 : -1;
			const _stepTime = Math.abs(Math.floor(_duration / _range));
			const _obj = el;

			const timer = setInterval(function() {
				_current += _increment;
				_obj.innerHTML = _current;

				if (_current == _end) clearInterval(timer);
			}, _stepTime);
		});
	};

	let _isActive = true;
	const getScrollPosition = () => {
		const _parent = document.querySelector('.counters__item--wrap');
		let _offset = _parent.getBoundingClientRect().y;
		const _height = document.documentElement.clientHeight;
		if (_offset < _height && _isActive) {
			animateValue('.js-counter');
			_isActive = false;
		}
	};

	const _item = document.querySelector('.counters__item--wrap');
	if (_item) {
		window.addEventListener('scroll', getScrollPosition);
	}
})();

//graphic animation
(function () {
	const _parentGr = document.querySelector('.graphic');
	if (_parentGr) {
		const _item = document.querySelectorAll('.js-graphicAnimation');
		const _heightDocument = document.documentElement.clientHeight;
		Array.prototype.forEach.call(_item, el => {
			const _heightParent = el.clientHeight;
			const _startPoint = _heightDocument - _heightParent + 100;
			let _isActive = true;
			window.addEventListener('scroll', () => {
				let _offset = el.getBoundingClientRect().y;
				if (_offset < _startPoint && _isActive) {
					el.classList.add('active');
					_isActive = false;
				}
			});
		})

	}
})();

// graphic mobile content
(function () {
	const _circleBtn = document.querySelectorAll('.js-circleBtn');

	Array.prototype.forEach.call(_circleBtn, el => {
		const _parent = el.parentNode;
		const _text = _parent.querySelector('.js-circleText');
		el.addEventListener('click', ev => {
			ev.preventDefault();
			const _allText = document.querySelectorAll('.js-circleText');
			_allText.forEach(element => {
				element.classList.remove('active');
			});
			_circleBtn.forEach(element => {
				element.classList.remove('active');
			});
			el.classList.add('active');
			_text.classList.add('active');
		})
	})
})();
